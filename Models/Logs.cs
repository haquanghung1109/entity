﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Logs
    {
        
        public int Id { get; set; } 
        public Transactions? Transactions {  get; set; }   
        public int LoginDate { get; set; }
        public DateTime LoginTime { get; set;}
        public string AddTextHere { get; set; }
    }
}
