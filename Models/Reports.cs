﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Reports
    {
        
        public int Id { get; set; }
        public Accounts? Account {  get; set; } 
        public Logs? Logs { get; set; } 
        public Transactions? Transactional {get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }
        public string AddTextHere { get; set; } 


    }
}
