﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Accounts
    {
        
        public int Id { get; set; }
        public Customer? Customer { get; set; }
        public string AcountName { get; set; }
        public string AddTextHere { get; set;}
    }
}
