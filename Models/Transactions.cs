﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Transactions
    {
        
        public int Id { get; set; }
        public Employees? Employee { get; set; } 
        public Customer? Customer { get; set; }
        public string Name { get; set; }    
        public string AddTextHere { get; set; }
    }
}
