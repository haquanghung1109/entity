﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Employees
    {
       
        public int Id { get; set; } 
        public string FirstName { get; set; }   
        public string LastName { get; set; }
        public string ContactAndAddress { get; set; }   
        public string UserNameAndPassWord { get; set; }
        public string AddTextHere { get; set; }

    }
}
